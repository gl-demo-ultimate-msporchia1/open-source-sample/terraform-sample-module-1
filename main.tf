provider "aws" {
  region = var.aws_region
}

resource "aws_s3_bucket" "example" {
  bucket = "sporchia-terraform-${var.env_name}"

  tags = {
    Environment = var.env_name
  }
}